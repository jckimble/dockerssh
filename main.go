package main

import (
	"log"
	"strings"

	"github.com/gorilla/mux"
	"net/http"
	"time"

	"github.com/spf13/viper"
	"gitlab.com/jckimble/dockerssh/dssh"
	"gitlab.com/jckimble/dockerssh/dssh/key"
	"gitlab.com/jckimble/dockerssh/locks"
	"gitlab.com/jckimble/dockerssh/models"
	"gitlab.com/jckimble/dockerssh/web"

	"github.com/docker/docker/client"
	"github.com/docker/go-connections/tlsconfig"
	"path/filepath"

	"math/rand"

	"gitlab.com/jckimble/go-health"
	"os"

	"context"
	"os/signal"
)

func randomString(len int) string {
	bytes := make([]byte, len)
	for i := 0; i < len; i++ {
		bytes[i] = byte(65 + rand.Intn(25)) //A=65 and Z = 65+25

	}
	return string(bytes)
}
func init() {
	viper.SetConfigName("dockerssh")
	viper.AddConfigPath("/etc/dockerssh/")
	viper.AddConfigPath("$HOME/.dockerssh")
	viper.AddConfigPath(".")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
	viper.SetDefault("userdb.lock.type", "memory")
	viper.SetDefault("userdb.type", "gorm")
	viper.SetDefault("ssh.enabled", true)
	viper.SetDefault("ssh.port", ":2222")
	viper.SetDefault("web.enabled", true)
	viper.SetDefault("web.port", ":8080")
	viper.SetDefault("web.jwtkey", randomString(32))
	viper.SetDefault("web.registration.enabled", false)

	viper.SetDefault("docker.passthrough", "none") //none,host,(docker.host)
	viper.SetDefault("docker.cert.path", "")
	viper.SetDefault("docker.tls.verify", "")
	viper.SetDefault("docker.host", "")
	viper.SetDefault("docker.api.version", "")

	err := viper.ReadInConfig()
	if err != nil {
		log.Printf("Unable to find config: %s\n", err)
	}
}

func main() {
	if len(os.Args) > 1 && os.Args[1] == "healthcheck" {
		port := viper.GetString("web.port")
		code, err := health.Healthcheck(os.Args[1:], port[1:], false)
		if err != nil {
			log.Printf("Healthcheck Error: %s", err)
		}
		os.Exit(code)
	}
	userDB, err := models.GetManager(viper.GetString("userdb.type"))
	if err != nil {
		log.Fatalf("Error: %s", err)
	}
	lockManager, err := locks.GetManager(viper.GetString("userdb.lock.type"))
	var web *http.Server
	var webe chan error
	if viper.GetBool("web.enabled") {
		web, webe = webServer(userDB, lockManager)
		if web == nil {
			log.Fatalf("Error: %s", <-webe)
		}
	} else {
		web, webe = healthServer()
		if web == nil {
			log.Fatalf("Error: %s", <-webe)
		}
	}
	var ssh *dssh.Server
	var sshe chan error
	if viper.GetBool("ssh.enabled") {
		ssh, sshe = sshServer(userDB, lockManager)
		if ssh == nil {
			log.Fatalf("Error: %s", <-sshe)
		}
	}
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	for {
		select {
		case <-c:
			log.Printf("Shutting Down")
			web.Shutdown(context.Background())
			if ssh != nil {
				ssh.Shutdown()
			}
			return
		case err := <-webe:
			log.Printf("Error: %s", err)
		case err := <-sshe:
			log.Printf("Error: %s", err)
		}
	}
}
func webServer(userDB models.UserManager, lockManager locks.LockManager) (*http.Server, chan error) {
	webpanel := web.WebPanel{
		UserDB: userDB,
	}
	api := mux.NewRouter()
	api.HandleFunc("/api/v1/users", webpanel.GetUsers).Methods("GET")
	api.HandleFunc("/api/v1/users", webpanel.AddUser).Methods("PUT")
	api.HandleFunc("/api/v1/users/{id}", webpanel.GetUser).Methods("GET")
	api.HandleFunc("/api/v1/users/{id}", webpanel.EditUser).Methods("POST")
	api.HandleFunc("/api/v1/users/{id}", webpanel.DeleteUser).Methods("DELETE")
	api.HandleFunc("/api/v1/users/{id}/keys", webpanel.GetUserKeys).Methods("GET")
	api.HandleFunc("/api/v1/users/{id}/keys", webpanel.AddUserKey).Methods("PUT")
	api.HandleFunc("/api/v1/users/{id}/keys/{fingerprint}", webpanel.DeleteKey).Methods("DELETE")
	api.HandleFunc("/api/v1/users/{id}/totp", webpanel.GenerateTotp).Methods("GET")
	api.HandleFunc("/api/v1/users/{id}/totp", webpanel.ActivateTotp).Methods("POST")
	api.HandleFunc("/api/v1/users/{id}/totp", webpanel.RemoveTotp).Methods("DELETE")
	r := mux.NewRouter()
	r.HandleFunc("/health", health.Check).Host("127.0.0.1" + viper.GetString("web.port"))

	auth := web.Auth{
		UserDB:      userDB,
		LockManager: lockManager,
	}
	r.HandleFunc("/api/login", auth.Signin).Methods("POST")
	r.HandleFunc("/api/authorize", auth.Authorize).Methods("POST")
	r.HandleFunc("/api/register", auth.Register).Methods("POST")
	r.PathPrefix("/api/v1/").Handler(web.AuthHandler{Handler: api})
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./dist/"))).Methods("GET")

	r.NotFoundHandler = http.HandlerFunc(webpanel.NotFound)
	api.NotFoundHandler = http.HandlerFunc(webpanel.NotFound)

	srv := &http.Server{
		Addr:         viper.GetString("web.port"),
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
		Handler:      web.JSONDataHandler{Handler: r},
	}
	e := make(chan error)
	go func() {
		log.Printf("Started Http Server %s", viper.GetString("web.port"))
		e <- srv.ListenAndServe()
	}()
	return srv, e
}
func healthServer() (*http.Server, chan error) {
	h := mux.NewRouter()
	h.HandleFunc("/health", health.Check).Host("127.0.0.1" + viper.GetString("web.port"))
	srv := &http.Server{
		Addr:    viper.GetString("web.port"),
		Handler: h,
	}
	e := make(chan error)
	go func() {
		log.Printf("Started HealthCheck Server %s", viper.GetString("web.port"))
		e <- srv.ListenAndServe()
	}()
	return srv, e
}
func fromViper(c *client.Client) error {
	if certPath := viper.GetString("docker.cert.path"); certPath != "" {
		options := tlsconfig.Options{
			CAFile:             filepath.Join(certPath, "ca.pem"),
			CertFile:           filepath.Join(certPath, "cert.pem"),
			KeyFile:            filepath.Join(certPath, "key.pem"),
			InsecureSkipVerify: viper.GetString("docker.tls.verify") == "",
		}
		tlsc, err := tlsconfig.Client(options)
		if err != nil {
			return err
		}
		httpclient := &http.Client{
			Transport:     &http.Transport{TLSClientConfig: tlsc},
			CheckRedirect: client.CheckRedirect,
		}
		if err := client.WithHTTPClient(httpclient)(c); err != nil {
			return err
		}
	}
	if host := viper.GetString("docker.host"); host != "" {
		if err := client.WithHost(host)(c); err != nil {
			return err
		}
	}
	if version := viper.GetString("docker.api.version"); version != "" {
		if err := client.WithVersion(version)(c); err != nil {
			return err
		}
	}
	return nil
}
func sshServer(userDB models.UserManager, lockManager locks.LockManager) (*dssh.Server, chan error) {
	e := make(chan error)
	docker, err := client.NewClientWithOpts(fromViper)
	if err != nil {
		e <- err
		return nil, e
	}
	srv := &dssh.Server{
		Docker:      docker,
		UserDB:      userDB,
		LockManager: lockManager,

		Addr: viper.GetString("ssh.port"),
	}
	if viper.GetString("ssh.key") != "" {
		viper.Set("ssh.storage.type", "file")
		viper.Set("ssh.storage.file", viper.GetString("ssh.key"))
	}
	if viper.GetString("ssh.storage.type") != "" {
		srv.KeyStorage, err = key.GetKeyStorage(viper.GetString("ssh.storage.type"))
		if err != nil {
			e <- err
			return nil, e
		}
	}
	go func() {
		log.Printf("Started SSH Server %s", viper.GetString("ssh.port"))
		e <- srv.ListenAndServe()
	}()
	return srv, e
}
