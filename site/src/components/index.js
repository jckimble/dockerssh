import FormGroupInput from "./FormGroupInput.vue";

import Button from "./Button";

import Modal from "./Modal"

import Card from "./Card.vue";

import SidebarPlugin from "./SidebarPlugin/index";

import VueQrcode from '@chenfengyuan/vue-qrcode';

let components = {
  FormGroupInput,
  Card,
  Button,
  Modal,
  SidebarPlugin,
  VueQrcode
};

export default components;
export {
  FormGroupInput,
  Card,
  Button,
  Modal,
  SidebarPlugin,
  VueQrcode
};
