import store from "../store.js";

import Vue from "vue";
import VueRouter from "vue-router";
import routes from "./routes";
Vue.use(VueRouter);

// configure router
const router = new VueRouter({
  routes, // short for routes: routes
  linkActiveClass: "active"
});

router.beforeEach(function(to, from, next) {
   if (to.matched.some(function(record) {
        return record.meta.requiresAuth;
      })) {
      if (!store.getters.isLoggedIn) {
        next({ name: "login" });
        return;
      }
    }
    if (to.matched.some(function(record) {
        return record.meta.requiresAdmin;
      })) {
      if (!store.getters.isAdmin) {
        next({ name: "userprofile" });
        return;
      }
    }
      next();
});
export default router;
