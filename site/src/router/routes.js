import DashboardLayout from "@/layout/DashboardLayout.vue";
import CenteredLayout from "@/layout/CenteredLayout.vue";
// GeneralViews
import Login from "@/pages/LoginPage.vue";
import Register from "@/pages/RegisterPage.vue";
import Authenticate from "@/pages/TotpPage.vue";

// Admin pages
import UserProfile from "@/pages/UserProfile.vue";
import Users from "@/pages/Users.vue";

const routes = [
  {
    path: "/",
    component: DashboardLayout,
    redirect: { name: "userprofile" },
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: "/profile",
        name: "userprofile",
        component: UserProfile
      },
      {
        path: "/profile/:userid",
        name: "profile",
        props: true,
        component: UserProfile,
        meta: {
          requiresAdmin: true
        }
      },
      {
        path: "/users",
        name: "users",
        component: Users,
        meta: {
          requiresAdmin: true
        }
      }
    ]
  },
  {
    path: "*",
    component: CenteredLayout,
    children: [
      {
        path: "/login",
        name: "login",
        component: Login
      },
      {
        path: "/register",
        name: "register",
        component: Register
      },
	  {
		path:"/authenticate",
		name: "auth",
		component: Authenticate
	  },
      { path: "*", redirect: { name: "userprofile" } }
    ]
  }
];

/**
 * Asynchronously load view (Webpack Lazy loading compatible)
 * The specified component must be inside the Views folder
 * @param  {string} name  the filename (basename) of the view to load.
function view(name) {
   var res= require('../components/Dashboard/Views/' + name + '.vue');
   return res;
};**/

export default routes;
