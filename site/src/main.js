import Vue from "vue";
import store from "./store";
import Axios from "axios";
import App from "./App";
import router from "./router/index";

import PaperDashboard from "./plugins/paperDashboard";
import "vue-notifyjs/themes/default.css";

Vue.use(PaperDashboard);

Vue.prototype.$http = Axios;
const token = localStorage.getItem("token");
if (token) {
  Vue.prototype.$http.defaults.headers.common["Authorization"] =
    "Bearer " + token;
}
Vue.prototype.$http.interceptors.response.use(undefined, function (err) {
  return new Promise(function(resolve, reject) {
    if (err.response.status == 401) {
      store.dispatch("logout").then(function(){
		  router.push({name:"login"})
	  });
    }
    throw err;
  });
});

/* eslint-disable no-new */
new Vue({
  store,
  router,
  render: h => h(App)
}).$mount("#app");
