import { FormGroupInput, Card, Button, Modal, VueQrcode } from "../components/index";
/**
 * You can register global components here and use them as a plugin in your main Vue instance
 */

const GlobalComponents = {
  install(Vue) {
    Vue.component("fg-input", FormGroupInput);
    Vue.component("card", Card);
    Vue.component("p-button", Button);
	Vue.component(VueQrcode.name, VueQrcode);
    Vue.component("p-modal",Modal)
  }
};

export default GlobalComponents;
