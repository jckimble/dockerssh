import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import api from './api/index.js';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    token: localStorage.getItem("token") || ""
  },
  mutations: {
    auth_success(state, token) {
      state.token = token;
    },
    logout(state) {
      state.token = "";
	}
  },
  actions: {
    login({ commit }, user) {
      return new Promise(function(resolve, reject) {
		api.userLogin(user)
          .then(function(data) {
            localStorage.setItem("token", data.token);
            axios.defaults.headers.common["Authorization"] =
              "Bearer " + data.token;
            commit("auth_success", data.token);
            resolve(data);
          })
          .catch(function(err) {
            localStorage.removeItem("token");
			reject(err)
          });
      });
    },
	authorize({commit},form){
	  return new Promise(function(resolve,reject){
		api.authorizeUser(form)
		  .then(function(data){
			  localStorage.setItem("token",data.token);
              axios.defaults.headers.common["Authorization"] =
                "Bearer " + data.token;
              commit("auth_success", data.token);
			  resolve(data)
		  })
		  .catch(function(err){
			  localStorage.removeItem("token")
			  reject(err)
		  })
	  });
	},
    register({ commit }, user) {
      return new Promise(function(resolve, reject) {
		api.registerUser(user)
          .then(function(data) {
            localStorage.setItem("token", data.token);
            axios.defaults.headers.common["Authorization"] =
              "Bearer " + data.token;
            commit("auth_success", data.token);
            resolve(data);
          })
          .catch(function(err) {
            localStorage.removeItem("token");
			reject(err)
          });
      });
    },
    logout({ commit }) {
      return new Promise(function(resolve, reject) {
        commit("logout");
        localStorage.removeItem("token");
        delete axios.defaults.headers.common["Authorization"];
        resolve();
      });
    }
  },
  getters: {
    isLoggedIn(state) {
	  if(state.token!=undefined && state.token!=""){
		  var spl=state.token.split('.')
		  if(spl.length==3){
		    var j=JSON.parse(atob(spl[1]))
			return j.authorized
		  }
	  }
      return false;
    },
    isAdmin(state) {
	  if(state.token!=undefined && state.token!=""){
		  var spl=state.token.split('.')
		  if(spl.length==3){
		    var j=JSON.parse(atob(spl[1]))
		    if(j.type=="Administrator"){
			  return true
		    }
		  }
	  }
      return false;
    },
    getUserId(state) {
	  if(state.token != undefined && state.token!=""){
		  var spl=state.token.split('.')
		  if(spl.length==3){
		    var j=JSON.parse(atob(spl[1]))
		    return j.id
		  }
	  }
	  return 0
    }
  }
});

export default store;
