import axios from "axios";
const server={
	getUser(userid){
		return new Promise(function(resolve,reject){
			axios({url:"/api/v1/users/"+userid,method:"GET"})
			.then(function(resp){
				resolve(resp.data)
			}).catch(function(err){
				if(err.response!=undefined && err.response.headers["content-type"]==="application/json"){
					reject(err.response.data.message)
				}else{
					reject("We're Having Problems Contacting The Server! Please Try Again Later.")
				}
			})
		})
	},
	updateUser(userid,user){
		return new Promise(function(resolve,reject){
			axios({url:"/api/v1/users/"+userid,data:user,method:"POST"})
			.then(function(resp){
				resolve(resp.data)
			}).catch(function(err){
				if(err.response!=undefined && err.response.headers["content-type"]==="application/json"){
					reject(err.response.data.message)
				}else{
					reject("We're Having Problems Contacting The Server! Please Try Again Later.")
				}
			})
		})
	},
	getUsers(){
		return new Promise(function(resolve,reject){
			axios({url:"/api/v1/users",method:"GET"})
			.then(function(resp){
				resolve(resp.data)
			}).catch(function(err){
				if(err.response!=undefined && err.response.headers["content-type"]==="application/json"){
					reject(err.response.data.message)
				}else{
					reject("We're Having Problems Contacting The Server! Please Try Again Later.")
				}
			})
		})
	},
	createUser(user){
		return new Promise(function(resolve,reject){
			axios({url:"/api/v1/users",data:user,method:"PUT"})
			.then(function(resp){
				resolve(resp.data)
			}).catch(function(err){
				if(err.response!=undefined && err.response.headers["content-type"]==="application/json"){
					reject(err.response.data.message)
				}else{
					reject("We're Having Problems Contacting The Server! Please Try Again Later.")
				}
			})
		})
	},
	deleteUser(id){
		return new Promise(function(resolve,reject){
			axios({url:"/api/v1/users/"+id,method: "DELETE"})
			.then(function(resp){
				resolve(resp.data)
			}).catch(function(err){
				if(err.response!=undefined && err.response.headers["content-type"]==="application/json"){
					reject(err.response.data.message)
				}else{
					reject("We're Having Problems Contacting The Server! Please Try Again Later.")
				}
			})
		})
	},
	userLogin(user){
		return new Promise(function(resolve,reject){
			axios({ url: "/api/login", data: user, method: "POST" })
			.then(function(resp){
				resolve(resp.data)
			}).catch(function(err){
				if(err.response!=undefined && err.response.headers["content-type"]==="application/json"){
					reject(err.response.data.message)
				}else{
					reject("We're Having Problems Contacting The Server! Please Try Again Later.")
				}
			})
		})
	},
	authorizeUser(form){
		return new Promise(function(resolve,reject){
			axios({url: "/api/authorize",data:form,method:"POST"})
			.then(function(resp){
				resolve(resp.data)
			}).catch(function(err){
				if(err.response!=undefined && err.response.headers["content-type"]==="application/json"){
					reject(err.response.data.message)
				}else{
					reject("We're Having Problems Contacting The Server! Please Try Again Later.")
				}
			})
		})
	},
	registerUser(user){
		return new Promise(function(resolve,reject){
			axios({ url: "/api/register", data: user, method: "POST" })
			.then(function(resp){
				resolve(resp.data)
			}).catch(function(err){
				if(err.response!=undefined && err.response.headers["content-type"]==="application/json"){
					reject(err.response.data.message)
				}else{
					reject("We're Having Problems Contacting The Server! Please Try Again Later.")
				}
			})
		})
	},
	getUsersKeys(userid){
		return new Promise(function(resolve,reject){
			axios({ url: "/api/v1/users/" + userid + "/keys", method: "GET" })
			.then(function(resp){
				resolve(resp.data)
			}).catch(function(err){
				if(err.response!=undefined && err.response.headers["content-type"]==="application/json"){
					reject(err.response.data.message)
				}else{
					reject("We're Having Problems Contacting The Server! Please Try Again Later.")
				}
			})
		})
	},
	deleteUserKey(userid,key){
		return new Promise(function(resolve,reject){
			axios({url: "/api/v1/users/" + userid + "/keys/" + key,method: "DELETE"})
			.then(function(resp){
				resolve(resp.data)
			}).catch(function(err){
				if(err.response!=undefined && err.response.headers["content-type"]==="application/json"){
					reject(err.response.data.message)
				}else{
					reject("We're Having Problems Contacting The Server! Please Try Again Later.")
				}
			})
		})
	},
	addUsersKey(userid,data){
		return new Promise(function(resolve,reject){
			axios({url: "/api/v1/users/" + userid + "/keys",data: data,method: "PUT"})
			.then(function(resp){
				resolve(resp.data)
			}).catch(function(err){
				if(err.response!=undefined && err.response.headers["content-type"]==="application/json"){
					reject(err.response.data.message)
				}else{
					reject("We're Having Problems Contacting The Server! Please Try Again Later.")
				}
			})
		})
	},

	generateTotp(userid){
		return new Promise(function(resolve,reject){
			axios({url:"/api/v1/users/"+userid+"/totp",method:"GET"})
			.then(function(resp){
				resolve(resp.data)
			}).catch(function(err){
				if(err.response!=undefined && err.response.headers["content-type"]==="application/json"){
					reject(err.response.data.message)
				}else{
					reject("We're Having Problems Contacting The Server! Please Try Again Later.")
				}
			})
		})
	},
	enableTotp(userid,data){
		return new Promise(function(resolve,reject){
			axios({url:"/api/v1/users/"+userid+"/totp",data:data,method:"POST"})
			.then(function(resp){
				resolve(resp.data)
			}).catch(function(err){
				if(err.response!=undefined && err.response.headers["content-type"]==="application/json"){
					reject(err.response.data.message)
				}else{
					reject("We're Having Problems Contacting The Server! Please Try Again Later.")
				}
			})
		})
	},
	disableTotp(userid){
		return new Promise(function(resolve,reject){
			axios({url:"/api/v1/users/"+userid+"/totp",method:"DELETE"})
			.then(function(resp){
				resolve(resp.data)
			}).catch(function(err){
				if(err.response!=undefined && err.response.headers["content-type"]==="application/json"){
					reject(err.response.data.message)
				}else{
					reject("We're Having Problems Contacting The Server! Please Try Again Later.")
				}
			})
		})
	}
}

export default server
