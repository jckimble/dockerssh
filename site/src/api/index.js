import mock from './mock.js'
import server from './server.js'

var api
if(process.env.NODE_ENV === 'development'){
	api=mock
}else{
	api=server
}

export default api
