const mock={
	user:{
		id:1,
		type:'Administrator',
		email:'johndoe@example.com',
		image:'registry.example.com/johndoe/dotfiles',
		username:'johndoe',
		lastname:'Doe',
		firstname:'John',
		disablePassword: true,
		totpEnabled:false,
		keys:[
			{
				name:"laptop",
				fingerprint: "43:51:43:a1:b5:fc:8b:b7:0a:3a:a9:b1:0f:66:73:a8",
				date:"Aug 01, 2018 16:54",
			}
		],		
	},
	getUser(userid){
		var self=this
		return new Promise(function(resolve,reject){
			resolve(self.user)
		})
	},
	updateUser(userid,user){
		return new Promise(function(resolve,reject){
			reject("Unable to update User")
		})
	},
	getUsers(){
		var self=this
		return new Promise(function(resolve,reject){
			resolve([self.user])
		})
	},
	createUser(user){
		return new Promise(function(resolve,reject){
			resolve(user)
		})
	},
	deleteUser(id){
		return new Promise(function(resolve,reject){
			reject("Unable to delete User")
		})
	},
	userLogin(user){
		var self=this
		return new Promise(function(resolve,reject){
			resolve({token:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiaWQiOjEsImF1dGhvcml6ZWQiOmZhbHNlLCJ0eXBlIjoiQWRtaW5pc3RyYXRvciIsImlhdCI6MTUxNjIzOTAyMn0.6EB0C3Av2CIlo_rnpAzeG6rzpe3ZSCwT0jHyOxmfOfo"})
		})
	},
	authorizeUser(form){
		return new Promise(function(resolve,reject){
			resolve({token:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwiaWQiOjEsImF1dGhvcml6ZWQiOnRydWUsInR5cGUiOiJBZG1pbmlzdHJhdG9yIiwiaWF0IjoxNTE2MjM5MDIyfQ.3mJ0kzF-UEy9o3xX8v5PH_ZOCOLeIovy4pqLtX1eMFc"})
		})
	},
	registerUser(user){
		var self=this
		return new Promise(function(resolve,reject){
			resolve(self.user)
		})
	},
	getUsersKeys(userid){
		var self=this
		return new Promise(function(resolve,reject){
			resolve(self.user.keys)
		})
	},
	deleteUserKey(userid,key){
		return new Promise(function(resolve,reject){
			reject("Unable to delete Key")
		})
	},
	addUsersKey(userid,data){
		return new Promise(function(resolve,reject){
			reject("Unable to add Key")
		})
	},

	generateTotp(userid){
		return new Promise(function(resolve,reject){
			resolve({signedSecret:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzZWNyZXQiOiJBQkNEIiwicGVyaW9kIjozMCwiYWxnb3JpdGhtIjoiU0hBMSIsImRpZ2l0cyI6NiwiaWF0IjoxNTE2MjM5MDIyfQ.ouCdiTUFi0RQ1aKNdwlc9kMNE_X95Sogn6JM002aSPs"})
		})
	},
	enableTotp(userid,totp){
		return new Promise(function(resolve,reject){
			resolve({})
		})
	},
	disableTotp(userid){
		return new Promise(function(resolve,reject){
			resolve({})
		})
	}
}

export default mock
