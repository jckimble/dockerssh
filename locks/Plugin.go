package locks

import "fmt"

var Managers = map[string]LockManager{}

func GetManager(name string) (LockManager, error) {
	if um, ok := Managers[name]; ok {
		if err := um.Init(); err != nil {
			return nil, err
		}
		return um, nil
	}
	return nil, fmt.Errorf("No LockManager with that type Registered!")
}
func RegisterManager(name string, manager LockManager) {
	Managers[name] = manager
}
