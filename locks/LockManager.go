package locks

type LockManager interface {
	AddFail(uint)
	IsLocked(uint) bool

	Init() error
}
