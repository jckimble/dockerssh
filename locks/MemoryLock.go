package locks

import (
	"github.com/patrickmn/go-cache"
	"strconv"
	"time"

	"golang.org/x/time/rate"
)

func init() {
	RegisterManager("memory", &MemoryLock{})
}

type lock struct {
	Locked bool

	limiter *rate.Limiter
}

func (l *lock) Increment() {
	if l.limiter == nil {
		l.limiter = rate.NewLimiter(rate.Every(15*time.Second), 1)
	}
	if !l.limiter.Allow() {
		l.Locked = true
	}
}

type MemoryLock struct {
	cache *cache.Cache
}

func (ml *MemoryLock) Init() error {
	ml.cache = cache.New(5*time.Minute, 15*time.Minute)
	return nil
}
func (ml *MemoryLock) AddFail(user uint) {
	userid := strconv.Itoa(int(user))
	var l *lock
	if val, found := ml.cache.Get(userid); found {
		if lock, ok := val.(*lock); ok {
			l = lock
		}
	}
	if l == nil {
		l = &lock{}
	}
	l.Increment()
	if l.Locked {
		ml.cache.Set(userid, l, 30*time.Minute)
	} else {
		ml.cache.SetDefault(userid, l)
	}
}

func (ml *MemoryLock) IsLocked(user uint) bool {
	userid := strconv.Itoa(int(user))
	if val, found := ml.cache.Get(userid); found {
		if lock, ok := val.(*lock); ok {
			return lock.Locked
		}
	}
	return false
}
