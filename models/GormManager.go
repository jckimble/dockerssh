package models

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mssql"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/mattn/go-sqlite3"

	"github.com/spf13/viper"
	"gitlab.com/jckimble/go-health"
)

func init() {
	RegisterManager("gorm", &GormManager{})
}

type GormManager struct {
	dbType string
	dbConn string
}

func (gm *GormManager) Init() error {
	viper.SetDefault("userdb.gorm.type", "sqlite3")
	viper.SetDefault("userdb.gorm.conn", "./testdb.sqlite")
	gm.dbType = viper.GetString("userdb.gorm.type")
	gm.dbConn = viper.GetString("userdb.gorm.conn")
	db, err := gorm.Open(gm.dbType, gm.dbConn)
	if err != nil {
		return err
	}
	defer db.Close()
	db.LogMode(false)
	if err := db.AutoMigrate(&User{}).Error; err != nil {
		return err
	}
	if err := db.AutoMigrate(&Key{}).Error; err != nil {
		return err
	}
	health.RegisterHealthCheck("DB", gm.healthCheck)
	return nil
}

func (gm GormManager) healthCheck() (int, error) {
	db, err := gorm.Open(gm.dbType, gm.dbConn)
	if err != nil {
		return 500, err
	}
	defer db.Close()
	db.LogMode(false)
	return 200, nil
}

func (gm GormManager) IsEmpty() bool {
	db, err := gorm.Open(gm.dbType, gm.dbConn)
	if err != nil {
		return false
	}
	defer db.Close()
	db.LogMode(false)
	var count int64
	if err := db.Model(&User{}).Count(&count).Error; err != nil {
		return false
	}
	return count == 0
}

func (gm GormManager) GetByName(username string) (*User, error) {
	db, err := gorm.Open(gm.dbType, gm.dbConn)
	if err != nil {
		return nil, err
	}
	defer db.Close()
	db.LogMode(false)
	u := &User{}
	if err := db.Preload("Keys").Where("user_name=?", username).First(&u).Error; err != nil {
		return nil, err
	}
	u.UserDB = &gm
	return u, nil
}

func (gm GormManager) GetByEmail(email string) (*User, error) {
	db, err := gorm.Open(gm.dbType, gm.dbConn)
	if err != nil {
		return nil, err
	}
	defer db.Close()
	db.LogMode(false)
	u := &User{}
	if err := db.Preload("Keys").Where("email=?", email).First(&u).Error; err != nil {
		return nil, err
	}
	u.UserDB = &gm
	return u, nil
}

func (gm GormManager) GetAll() ([]*User, error) {
	var users []*User
	db, err := gorm.Open(gm.dbType, gm.dbConn)
	if err != nil {
		return nil, err
	}
	defer db.Close()
	db.LogMode(false)
	if err := db.Find(&users).Error; err != nil {
		return nil, err
	}
	for i, _ := range users {
		users[i].UserDB = &gm
	}
	return users, nil
}

func (gm GormManager) GetByID(userid uint) (*User, error) {
	db, err := gorm.Open(gm.dbType, gm.dbConn)
	if err != nil {
		return nil, err
	}
	defer db.Close()
	db.LogMode(false)
	u := &User{}
	if err := db.Preload("Keys").Where("id=?", userid).First(&u).Error; err != nil {
		return nil, err
	}
	u.UserDB = &gm
	return u, nil
}

func (gm GormManager) DeleteKey(k *Key) error {
	db, err := gorm.Open(gm.dbType, gm.dbConn)
	if err != nil {
		return err
	}
	defer db.Close()
	db.LogMode(false)
	return db.Model(&Key{}).Delete(k).Error
}

func (gm GormManager) Save(user *User) error {
	db, err := gorm.Open(gm.dbType, gm.dbConn)
	if err != nil {
		return err
	}
	defer db.Close()
	db.LogMode(false)
	return db.Save(&user).Error
}

func (gm GormManager) Delete(userid uint) error {
	db, err := gorm.Open(gm.dbType, gm.dbConn)
	if err != nil {
		return err
	}
	defer db.Close()
	db.LogMode(false)
	return db.Where("id = ?", userid).Delete(&User{}).Error
}
