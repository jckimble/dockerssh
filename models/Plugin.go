package models

import "fmt"

var Managers = map[string]UserManager{}

func GetManager(name string) (UserManager, error) {
	if um, ok := Managers[name]; ok {
		if err := um.Init(); err != nil {
			return nil, err
		}
		return um, nil
	}
	return nil, fmt.Errorf("No UserManager with that type Registered!")
}
func RegisterManager(name string, manager UserManager) {
	Managers[name] = manager
}
