package models

import (
	"golang.org/x/crypto/ssh"

	"bytes"
	"crypto/md5"
	"encoding/json"
	"fmt"
	"github.com/pquerna/otp/totp"
	"golang.org/x/crypto/bcrypt"
	"time"
)

type UserManager interface {
	Init() error

	GetByEmail(string) (*User, error)
	GetAll() ([]*User, error)
	GetByID(uint) (*User, error)
	Save(*User) error
	Delete(uint) error

	DeleteKey(*Key) error

	GetByName(string) (*User, error)
	IsEmpty() bool
}

type User struct {
	ID              uint   `json:"id"`
	Type            string `json:"type"`
	Email           string `json:"email" gorm:"unique"`
	Image           string `json:"image"`
	Password        []byte `json:"-"`
	FirstName       string `json:"firstname"`
	LastName        string `json:"lastname"`
	UserName        string `json:"username" gorm:"unique"`
	DisablePassword bool   `json:"disablePassword"`
	TotpToken       string `json:"-"`

	Keys []*Key `json:"-" gorm:"foreignkey:UserID"`

	UserDB UserManager `json:"-" gorm:"-"`
}

func (u User) MarshalJSON() ([]byte, error) {
	key := struct {
		ID              uint   `json:"id"`
		Type            string `json:"type"`
		Email           string `json:"email"`
		Image           string `json:"image"`
		FirstName       string `json:"firstname"`
		LastName        string `json:"lastname"`
		UserName        string `json:"username"`
		DisablePassword bool   `json:"disablePassword"`
		TotpEnabled     bool   `json:"totpEnabled"`
	}{
		ID:              u.ID,
		Type:            u.Type,
		Email:           u.Email,
		Image:           u.Image,
		FirstName:       u.FirstName,
		LastName:        u.LastName,
		UserName:        u.UserName,
		DisablePassword: u.DisablePassword,
		TotpEnabled:     u.TotpToken != "",
	}
	return json.Marshal(key)

}

func (u *User) SetPassword(pass string) (err error) {
	u.Password, err = bcrypt.GenerateFromPassword([]byte(pass), bcrypt.DefaultCost)
	return
}

func (u User) ValidateTotp(code string) bool {
	if u.TotpToken != "" {
		return totp.Validate(code, u.TotpToken)
	}
	return false
}

func (u User) ValidatePassword(pass string) bool {
	if u.Password != nil {
		if err := bcrypt.CompareHashAndPassword(u.Password, []byte(pass)); err == nil {
			return true
		}
	}
	return false
}

func (u User) ValidateSshKey(keydata []byte) bool {
	if publicKey, _, _, _, err := ssh.ParseAuthorizedKey(keydata); err == nil {
		if len(u.Keys) > 0 {
			for _, k := range u.Keys {
				if bytes.Equal(k.Data, publicKey.Marshal()) {
					return true
				}
			}
		}
	}
	return false
}
func (u *User) AddKey(k *Key) {
	u.Keys = append(u.Keys, k)
}
func (u *User) RemoveKey(fingerprint string) {
	for i, v := range u.Keys {
		if v.FingerPrint == fingerprint {
			u.UserDB.DeleteKey(v)
			u.Keys = append(u.Keys[:i], u.Keys[i+1:]...)
			return
		}
	}
}
func (u User) GetKeys() []*Key {
	return u.Keys
}

func (u User) IsValid() bool {
	return u.Image != "" && u.Password != nil && u.Email != ""
}

type Key struct {
	UserID      uint      `json:"-"`
	Name        string    `json:"name"`
	FingerPrint string    `json:"fingerprint"`
	Data        []byte    `json:"-"`
	Date        time.Time `json:"date"`
}

func (k Key) MarshalJSON() ([]byte, error) {
	key := struct {
		Name        string `json:"name"`
		FingerPrint string `json:"fingerprint"`
		Date        string `json:"date"`
	}{
		Name:        k.Name,
		FingerPrint: k.FingerPrint,
		Date:        k.Date.Format("Jan _2 2006"),
	}
	return json.Marshal(key)
}

func (k *Key) GenerateFingerprint() error {
	hash := md5.Sum(k.Data)
	out := ""
	for i := 0; i < 16; i++ {
		if i > 0 {
			out += ":"
		}
		out += fmt.Sprintf("%02x", hash[i]) // don't forget the leading zeroes
	}
	k.FingerPrint = out
	return nil
}
