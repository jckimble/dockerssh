FROM alpine:latest as npmbuild
RUN apk add --update nodejs git npm
COPY ./site/package.json /src/package.json
COPY ./site/vue.config.js /src/vue.config.js
COPY ./site/public /src/public
COPY ./site/src /src/src
WORKDIR /src
RUN npm install
RUN npm run build

FROM golang:alpine as gobuild
RUN apk add --update git gcc libc-dev
ADD . /go/src/gitlab.com/jckimble/dockerssh/
WORKDIR /go/src/gitlab.com/jckimble/dockerssh
RUN go get
RUN GOOS=linux go build -o dockerssh .

FROM alpine:latest
RUN mkdir -p /app
WORKDIR /app
COPY --from=gobuild /go/src/gitlab.com/jckimble/dockerssh/dockerssh .
COPY --from=npmbuild /src/dist/ ./dist/
EXPOSE 8080
EXPOSE 2222
VOLUME ["/var/run/docker.sock","/run/dockerssh"]
HEALTHCHECK CMD ["/app/dockerssh","healthcheck"]
CMD ["/app/dockerssh"]
