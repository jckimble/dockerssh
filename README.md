# Docker Ssh Server 
[![pipeline status](https://gitlab.com/jckimble/dockerssh/badges/master/pipeline.svg)](https://gitlab.com/jckimble/dockerssh/commits/master)

Docker Ssh Server is a ssh server to use docker images as an vm. Ideally used with dotfiles docker images.

---
* [Install](#install)
* [Configuration](#configuration)
* [Examples](#examples)
* [Notes](#notes)
* [RoadMap](#roadmap)
* [License](#license)

---

## Install
```sh
docker pull registry.gitlab.com/jckimble/dockerssh
```

## Configuration
This project uses viper so it supports any type viper does including environment variables. It searches for dockerssh.* in one of the following directories: /etc/dockerssh/, ~/.dockerssh/, and the current directory. Shown below are the defaults no configuration is needed if you are happy with the following.
```toml
[ssh]
  enabled = true
  port = ":2222"
  
  [storage]
    type="" #ssh identity key storage type (blank to not store,"file" to write to disk)
	file="" #file location for key to be stored

[userdb]
  type = "gorm"

  [userdb.gorm]
    conn = "./testdb.sqlite"
    type = "sqlite3"

[web]
  enabled = true
  jwtkey = "" # HMAC Key to issue jwt keys with, if blank key is random and generated on run
  port = ":8080"

  [web.registration]
    enabled = false

[docker]
  passthrough = "none" # none,host or connection info. links /var/run/docker.sock depending on value
  cert.path=""
  tls.verify=""
  host=""
  api.version=""
```

## Examples
Below is the most basic example, if you want more advanced examples look in the examples directory.
```sh
docker run -p 2222:2222 -p 8080:8080 -v /run/dockerssh:/run/dockerssh -v /var/run/docker.sock:/var/run/docker.sock registry.gitlab.com/jckimble/dockerssh
```

## Notes
* Do not use this in a public system. While I have looked for security exploits exploits like CVE-2017-5123 do exist. I suggest running this behind a vpn and with users you would trust with standard ssh access.
* If you do ignore this warning (someone will) at very least add a dns cache and block all outgoing ports possible.
* Please submit an issue if you have a feature request for this project, that way I can talk with you and see why the feature is an good idea along with possibily adding feedback to make it better.
* Port Forwarding does not work with dind

## RoadMap
This is a list of features I would like to add overtime.
* Remove Duplicate Code & Write Tests
* gpg-agent stub for non-forwared socket mounts 

## License

Copyright 2018 James Kimble

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
