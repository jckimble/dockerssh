package main

import (
	"golang.org/x/crypto/ssh/terminal"
	"log"
	"os"
	"os/exec"
	"os/signal"

	"fmt"
	"net"
	"net/rpc"
	"syscall"
	"time"
)

func main() {
	go func() {
		var sig = make(chan os.Signal, 3)
		signal.Notify(sig, syscall.SIGCHLD)
		for {
			<-sig
			var status syscall.WaitStatus
			if pid, err := syscall.Wait4(-1, &status, 0, nil); err == syscall.EINTR {
				syscall.Wait4(pid, &status, 0, nil)
			}
		}
	}() //reap dead proccesses
	handler := &MsgHandler{}
	handler.register("shutdown", handler.shutdown)

	if terminal.IsTerminal(int(os.Stdout.Fd())) {
		command := os.Args[1:]
		if len(command) > 0 {
			if handler.isHandled(command[0]) {
				msg, err := sendRequest(command)
				if err != nil {
					log.Fatalf("%s", err.Error())
				}
				log.Printf("%s", msg)
				return
			}
			cmd := exec.Command(command[0], command[1:]...)
			cmd.Stdin = os.Stdin
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			if err := cmd.Run(); err != nil {
				if _, ok := err.(*exec.ExitError); !ok {
					log.Printf("%s\n", err.Error())
				}
			}
		}
	} else {
		listener, err := net.Listen("unix", "/tmp/dsinit.sock")
		if err != nil {
			log.Printf("%s", err.Error())
		}
		defer listener.Close()
		handler.listener = listener
		srv := rpc.NewServer()
		srv.RegisterName("dsinit", handler)
		srv.Accept(listener)
	}
}

func sendRequest(command []string) (string, error) {
	client, err := rpc.Dial("unix", "/tmp/dsinit.sock")
	if err != nil {
		return "", err
	}
	defer client.Close()
	response := new(Response)
	if err := client.Call("dsinit.Execute", command, response); err != nil {
		return "", err
	}
	return response.Message, nil
}

type MsgHandler struct {
	listener net.Listener
	commands map[string]func([]string, *Response) error
}

func (m *MsgHandler) isHandled(command string) bool {
	if m.commands != nil {
		if m.commands[command] != nil {
			return true
		}
	}
	return false
}
func (m *MsgHandler) Execute(command []string, res *Response) error {
	if m.commands != nil {
		if m.commands[command[0]] != nil {
			return m.commands[command[0]](command[1:], res)
		}
	}
	return fmt.Errorf("Command %s Doesn't Exist", command[0])
}
func (m *MsgHandler) register(command string, f func([]string, *Response) error) {
	if m.commands == nil {
		m.commands = map[string]func([]string, *Response) error{}
	}
	m.commands[command] = f
}
func (m *MsgHandler) shutdown(args []string, res *Response) (err error) {
	duration := 3 * time.Second
	if len(args) == 1 {
		duration, err = time.ParseDuration(args[0])
		if err != nil {
			return err
		}
	}
	res.Message = fmt.Sprintf("Shutting Down in %s", duration.String())
	go func() {
		time.Sleep(duration)
		m.listener.Close()
	}()
	return nil
}

type Response struct {
	Message string
}
