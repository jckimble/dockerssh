package dssh

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strconv"
	"time"

	"encoding/json"

	"gitlab.com/jckimble/dockerssh/dssh/sock"
	"golang.org/x/crypto/ssh"
	"path"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/pkg/stdcopy"
)

func (s *Session) Forwarder() {
	user, err := s.UserDB.GetByName(s.User())
	if err != nil {
		fmt.Fprintln(s, err)
		log.Printf("Error: %s\n", err)
		s.Exit(128)
		return
	}
	cfg := &container.Config{
		Labels:       map[string]string{"dockerssh.user": s.User()},
		Image:        user.Image,
		Cmd:          s.Command(),
		Env:          s.Environ(),
		Tty:          s.pty,
		OpenStdin:    false,
		AttachStderr: false,
		AttachStdout: false,
		StdinOnce:    false,
		Volumes:      make(map[string]struct{}),
	}
	var id string
	id, err = s.findContainer(cfg)
	if err != nil {
		if err := s.downloadImage(user.Image); err != nil {
			fmt.Fprintln(s, err)
			log.Printf("Error: %s\n", err)
			s.Exit(128)
			return
		}
		id, err = s.createContainer(cfg)
		if err != nil {
			fmt.Fprintln(s, err)
			log.Printf("Error: %s\n", err)
			s.Exit(128)
			return
		}
	}
	s.containerid = id
	defer s.cleanupContainer(id)
	err = s.attachContainer(id, cfg)
	if err != nil {
		fmt.Fprintln(s, err)
		log.Printf("Cleanup Error: %s", err)
		s.Exit(128)
	}
}

func (s Session) findContainer(cfg *container.Config) (string, error) {
	filter := filters.NewArgs()
	filter.Add("status", "running")
	for k, v := range cfg.Labels {
		filter.Add("label", k+"="+v)
	}
	containers, err := s.Docker.ContainerList(context.Background(), types.ContainerListOptions{Filters: filter})
	if err != nil {
		return "", err
	}
	for _, c := range containers {
		return c.ID, nil
	}
	return "", fmt.Errorf("No Existing Container")
}

func (s Session) createContainer(cfg *container.Config) (string, error) {
	ctx := context.Background()
	tty := cfg.Tty
	cfg.Tty = false
	hostConfig := &container.HostConfig{
		Mounts: []mount.Mount{},
	}
	mounts, err := s.parseSockMounts(cfg.Image)
	if err != nil {
		return "", err
	}
	for _, mount := range mounts {
		skip := false
		for _, m := range hostConfig.Mounts {
			if mount.Target == m.Target {
				skip = true
				break
			}
		}
		if !skip {
			hostConfig.Mounts = append(hostConfig.Mounts, mount)
		}
	}
	res, err := s.Docker.ContainerCreate(ctx, cfg, hostConfig, nil, "")
	if err != nil {
		return "", err
	}
	err = s.Docker.ContainerStart(ctx, res.ID, types.ContainerStartOptions{})
	if err != nil {
		return "", err
	}
	cfg.Tty = tty
	return res.ID, nil
}

func (s *Session) forwardPort(port uint32, ch ssh.Channel) {
	container, err := s.Docker.ContainerInspect(context.Background(), s.containerid)
	if err != nil {
		ch.Close()
		log.Printf("Inspect Error: %s", err)
		return
	}
	dialer := net.Dialer{
		Timeout: 15 * time.Second,
	}
	conn, err := dialer.Dial("tcp", container.NetworkSettings.IPAddress+":"+strconv.Itoa(int(port)))
	if err != nil {
		ch.Close()
		return
	}
	go func() {
		defer conn.Close()
		defer ch.Close()
		io.Copy(conn, ch)
	}()
	defer conn.Close()
	defer ch.Close()
	io.Copy(ch, conn)
}

func (s Session) cleanupContainer(id string) error {
	container, err := s.Docker.ContainerInspect(context.Background(), id)
	if err != nil {
		return err
	}
	if container.ContainerJSONBase.State.Running {
		return nil
	}
	ctx := context.Background()
	return s.Docker.ContainerRemove(ctx, id, types.ContainerRemoveOptions{})
}

func (s Session) downloadImage(image string) error {
	ctx := context.Background()
	stream, err := s.Docker.ImagePull(ctx, image, types.ImagePullOptions{})
	if err != nil {
		return err
	}
	defer stream.Close()
	d := json.NewDecoder(stream)
	type Event struct {
		Status         string `json:"status"`
		Error          string `json:"error"`
		Progress       string `json:"progress"`
		ProgressDetail struct {
			Current int `json:"current"`
			Total   int `json:"total"`
		} `json:"progressDetail"`
	}
	var event *Event
	fmt.Fprintln(s)
	for {
		if err := d.Decode(&event); err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		if s.pty {
			fmt.Fprintf(s, "\r%s: %s            ", event.Status, event.Progress)
		} else {
			fmt.Fprintf(s, "%s: %s\n", event.Status, event.Progress)
		}
	}
	return nil
}

func (s Session) parseSockMounts(image string) ([]mount.Mount, error) {
	im, _, err := s.Docker.ImageInspectWithRaw(context.Background(), image)
	if err != nil {
		return nil, err
	}
	mounts := []mount.Mount{}
	if im.Config != nil {
		for k, _ := range im.Config.Volumes {
			key := sock.Key{
				User: s.User(),
				Base: path.Base(k),
			}
			forward := s.Server.GetForward(key)
			if !forward.IsServing() {
				if _, err := os.Stat("/run/dockerssh/" + key.User + "/" + key.Base); err == nil {
					os.Remove("/run/dockerssh/" + key.User + "/" + key.Base)
				}
				if err := os.MkdirAll("/run/dockerssh/"+key.User, 0700); err != nil {
					return nil, err
				}
				ln, err := net.Listen("unix", "/run/dockerssh/"+key.User+"/"+key.Base)
				if err != nil {
					return nil, err
				}
				forward.Lock()
				go func() {
					os.Chmod("/run/dockerssh/"+key.User+"/"+key.Base, 0777)
					if err := forward.Serve(sock.RmListener{Listener: ln, File: "/run/dockerssh/" + key.User + "/" + key.Base}); err != nil {
						if e, ok := err.(*net.OpError); !ok || e.Err.Error() != "use of closed network connection" {
							log.Printf("Socket Serve Error: %s", err)
						}
					}
				}()
			}
			mounts = append(mounts, mount.Mount{
				Type:   mount.TypeBind,
				Source: "/run/dockerssh/" + key.User + "/" + key.Base,
				Target: k,
			})
		}
	}
	if im.ContainerConfig != nil {
		for k, _ := range im.ContainerConfig.Volumes {
			key := sock.Key{
				User: s.User(),
				Base: path.Base(k),
			}
			forward := s.Server.GetForward(key)
			if !forward.IsServing() {
				if _, err := os.Stat("/run/dockerssh/" + key.User + "/" + key.Base); err == nil {
					os.Remove("/run/dockerssh/" + key.User + "/" + key.Base)
				}
				if err := os.MkdirAll("/run/dockerssh/"+key.User, 0700); err != nil {
					return nil, err
				}
				ln, err := net.Listen("unix", "/run/dockerssh/"+key.User+"/"+key.Base)
				if err != nil {
					return nil, err
				}
				go func() {
					os.Chmod("/run/dockerssh/"+key.User+"/"+key.Base, 0777)
					if err := forward.Serve(sock.RmListener{Listener: ln, File: "/run/dockerssh/" + key.User + "/" + key.Base}); err != nil {
						if e, ok := err.(*net.OpError); !ok || e.Err.Error() != "use of closed network connection" {
							log.Printf("Socket Serve Error: %s", err)
						}
					}
				}()
			}
			mounts = append(mounts, mount.Mount{
				Type:   mount.TypeBind,
				Source: "/run/dockerssh/" + key.User + "/" + key.Base,
				Target: k,
			})
		}
	}
	return mounts, nil
}

func (s Session) defaultImageCommand(image string) []string {
	im, _, err := s.Docker.ImageInspectWithRaw(context.Background(), image)
	if err != nil {
		return nil
	}
	if im.Config != nil {
		return im.Config.Cmd
	}
	if im.ContainerConfig != nil {
		return im.ContainerConfig.Cmd
	}
	return nil
}

func (s *Session) attachContainer(id string, cfg *container.Config) error {
	ctx := context.Background()
	config := types.ExecConfig{
		Tty:          cfg.Tty,
		AttachStdin:  true,
		AttachStderr: true,
		AttachStdout: true,
		Env:          cfg.Env,
		Cmd:          cfg.Cmd,
	}
	if len(config.Cmd) == 0 {
		config.Cmd = s.defaultImageCommand(cfg.Image)
	}
	res, err := s.Docker.ContainerExecCreate(ctx, id, config)
	if err != nil {
		return err
	}
	s.execid = res.ID
	stream, err := s.Docker.ContainerExecAttach(ctx, s.execid, types.ExecStartCheck{Tty: cfg.Tty})
	if err != nil {
		return err
	}
	defer stream.Close()

	go func() {
		defer s.Exit(0)
		if cfg.Tty {
			io.Copy(s, stream.Reader)
		} else {
			stdcopy.StdCopy(s, s.Stderr(), stream.Reader)
		}
	}()
	s.resizePty()
	defer stream.CloseWrite()
	defer s.Exit(0)
	io.Copy(stream.Conn, s)
	return nil
}
func (s Session) resizePty() {
	if !s.pty || s.execid == "" {
		return
	}
	err := s.Docker.ContainerExecResize(context.Background(), s.execid, types.ResizeOptions{
		Height: s.winHeight,
		Width:  s.winWidth,
	})
	if err != nil {
		log.Printf("%s", s.execid)
		log.Printf("Resize Error: %s", err)
		s.Close()
	}
}
