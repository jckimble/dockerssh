package dssh

import (
	"github.com/anmitsu/go-shlex"
	"golang.org/x/crypto/ssh"

	"gitlab.com/jckimble/dockerssh/models"

	"github.com/docker/docker/client"

	"encoding/binary"
	"fmt"
	"log"
)

type Session struct {
	UserDB models.UserManager
	Docker *client.Client

	Channel ssh.Channel
	Conn    ssh.Conn
	Server  *Server
	handled bool

	cmd         []string
	env         []string
	pty         bool
	execid      string
	containerid string

	winHeight uint
	winWidth  uint
}

func (s Session) Close() error {
	s.Channel.Close()
	return s.Conn.Close()
}

func (s Session) Stderr() Session {
	return s
}

func (s Session) Read(b []byte) (int, error) {
	return s.Channel.Read(b)
}

func (s Session) Write(b []byte) (int, error) {
	return s.Channel.Write(b)
}

func (s Session) Environ() []string {
	return s.env
}

func (s Session) Command() []string {
	return s.cmd
}

func (s Session) User() string {
	return s.Conn.User()
}

func (s Session) Exit(code int) error {
	status := struct{ Status uint32 }{uint32(code)}
	if _, err := s.Channel.SendRequest("exit-status", false, ssh.Marshal(&status)); err != nil {
		return err
	}
	return s.Close()
}

func (s *Session) handleRequests(reqs <-chan *ssh.Request) {
	for req := range reqs {
		switch req.Type {
		case "shell", "exec":
			if s.handled {
				req.Reply(false, nil)
				continue
			}
			s.handled = true
			req.Reply(true, nil)
			var payload = struct{ Value string }{}
			ssh.Unmarshal(req.Payload, &payload)
			s.cmd, _ = shlex.Split(payload.Value, true)
			go s.Forwarder()
		case "pty-req":
			s.pty = true
			termLen := req.Payload[3]
			s.winWidth, s.winHeight = s.parseDims(req.Payload[termLen+4:])
			req.Reply(true, nil)
		case "window-change":
			s.winWidth, s.winHeight = s.parseDims(req.Payload)
			s.resizePty()
		case "env":
			if s.handled {
				req.Reply(false, nil)
				continue
			}
			var kv struct{ Key, Value string }
			ssh.Unmarshal(req.Payload, &kv)
			if s.env == nil {
				s.env = []string{}
			}
			s.env = append(s.env, fmt.Sprintf("%s=%s", kv.Key, kv.Value))
			req.Reply(true, nil)
		default:
			log.Printf("Missing Type: %s", req.Type)
		}
	}
}
func (s Session) parseDims(b []byte) (uint, uint) {
	w := binary.BigEndian.Uint32(b)
	h := binary.BigEndian.Uint32(b[4:])
	return uint(w), uint(h)
}
