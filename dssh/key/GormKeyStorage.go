package key

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mssql"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/mattn/go-sqlite3"

	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"github.com/spf13/viper"
	"time"
)

func init() {
	RegisterKeyStorage("gorm", &GormKeyStorage{})
}

type SshKey struct {
	ID         uint `gorm:"primary_key"`
	CreatedAt  time.Time
	PrivateKey []byte
}

func (sk *SshKey) Decode() ([]byte, error) {
	if sk.PrivateKey == nil {
		return nil, fmt.Errorf("No Private Key")
	}
	return sk.PrivateKey, nil
}

func (sk *SshKey) Encode(key *rsa.PrivateKey) error {
	data := pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(key)})
	sk.PrivateKey = data
	return nil
}

type GormKeyStorage struct {
	dbType string
	dbConn string
}

func (gks *GormKeyStorage) Init() error {
	viper.SetDefault("userdb.gorm.type", "sqlite3")
	viper.SetDefault("userdb.gorm.conn", "./testdb.sqlite")
	gks.dbType = viper.GetString("userdb.gorm.type")
	gks.dbConn = viper.GetString("userdb.gorm.conn")
	db, err := gorm.Open(gks.dbType, gks.dbConn)
	if err != nil {
		return err
	}
	defer db.Close()
	db.LogMode(false)
	if err := db.AutoMigrate(&SshKey{}).Error; err != nil {
		return err
	}
	return nil
}

func (gks *GormKeyStorage) Get() ([]byte, error) {
	db, err := gorm.Open(gks.dbType, gks.dbConn)
	if err != nil {
		return nil, err
	}
	defer db.Close()
	db.LogMode(false)
	sshKey := &SshKey{}
	if err := db.First(&sshKey).Error; err != nil {
		return nil, err
	}
	return sshKey.Decode()
}

func (gks *GormKeyStorage) Save(key *rsa.PrivateKey) error {
	db, err := gorm.Open(gks.dbType, gks.dbConn)
	if err != nil {
		return err
	}
	defer db.Close()
	db.LogMode(false)
	sshKey := &SshKey{}
	if err := sshKey.Encode(key); err != nil {
		return err
	}
	return db.Save(&sshKey).Error
}
