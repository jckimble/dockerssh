package key

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"io/ioutil"
	"os"

	"github.com/spf13/viper"
)

func init() {
	RegisterKeyStorage("file", FileKeyStorage{})
}

type FileKeyStorage struct {
}

func (FileKeyStorage) Init() error {
	return nil
}

func (FileKeyStorage) Get() ([]byte, error) {
	key, err := ioutil.ReadFile(viper.GetString("ssh.storage.file"))
	if err != nil {
		return nil, err
	}
	return key, nil
}

func (FileKeyStorage) Save(key *rsa.PrivateKey) error {
	keyOut, err := os.Create(viper.GetString("ssh.storage.file"))
	if err != nil {
		return err
	}
	pem.Encode(keyOut, &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(key)})
	return keyOut.Close()
}
