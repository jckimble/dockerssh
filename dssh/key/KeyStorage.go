package key

import (
	"crypto/rsa"
	"fmt"
)

type KeyStorage interface {
	Get() ([]byte, error)
	Save(*rsa.PrivateKey) error
	Init() error
}

var keys = map[string]KeyStorage{}

func GetKeyStorage(name string) (KeyStorage, error) {
	if s, ok := keys[name]; ok {
		if err := s.Init(); err != nil {
			return nil, err
		}
		return s, nil
	}
	return nil, fmt.Errorf("No KeyStorage with that name")
}

func RegisterKeyStorage(name string, key KeyStorage) {
	keys[name] = key
}
