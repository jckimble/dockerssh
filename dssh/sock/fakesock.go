package sock

import (
	"net"
	"sync"

	"golang.org/x/crypto/ssh"
)

type Key struct {
	User string
	Base string
}

type FakeSock interface {
	Lock()

	GetFileBases() []string
	SetSocket(ssh.Conn, string)
	Serve(ln net.Listener) error
	Close() error
	IsServing() bool
}

var fakesocks = map[string]func() FakeSock{}

func RegisterFakeSock(fs func() FakeSock) {
	nfs := fs()
	bases := nfs.GetFileBases()
	for _, base := range bases {
		fakesocks[base] = fs
	}
}
func GetFakeSock(basename string) FakeSock {
	if fs, ok := fakesocks[basename]; ok {
		return fs()
	}
	return &DefaultSock{Mutex: new(sync.Mutex)}
}
