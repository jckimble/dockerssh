package sock

import (
	"io"
	"net"
	"sync"

	"golang.org/x/crypto/ssh"
)

func init() {
	RegisterFakeSock(func() FakeSock {
		return &DefaultSock{Mutex: new(sync.Mutex)}
	})
}

type DefaultSock struct {
	*sync.Mutex
	conn   ssh.Conn
	socket string

	ln net.Listener
}

func (gs *DefaultSock) SetSocket(conn ssh.Conn, socket string) {
	gs.conn = conn
	gs.socket = socket
}

func (gs DefaultSock) GetFileBases() []string {
	return []string{}
}

func (gs *DefaultSock) IsServing() bool {
	gs.Lock()
	defer gs.Unlock()
	return gs.ln != nil
}

func (gs *DefaultSock) Close() error {
	if gs.ln == nil {
		return nil
	}
	if err := gs.ln.Close(); err != nil {
		return err
	}
	gs.ln = nil
	return nil
}

func (gs *DefaultSock) Serve(ln net.Listener) error {
	gs.ln = ln
	gs.Unlock()
	defer gs.Close()
	for {
		conn, err := ln.Accept()
		if err != nil {
			return err
		}
		if gs.conn == nil {
			conn.Close()
		} else {
			ch, reqs, err := gs.conn.OpenChannel("forwarded-streamlocal@openssh.com", ssh.Marshal(struct {
				SocketPath string
				Reserved0  string
			}{
				SocketPath: gs.socket,
			}))
			if err != nil {
				return err
			}
			go ssh.DiscardRequests(reqs)
			go gs.bridgeConn(conn, ch)
			go gs.bridgeConn(ch, conn)
		}
	}
}
func (gs DefaultSock) bridgeConn(c1, c2 io.ReadWriter) {
	defer c1.(io.Closer).Close()
	defer c2.(io.Closer).Close()
	io.Copy(c1, c2)
}
