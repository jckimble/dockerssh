package sock

import (
	"io"
	"net"
	"net/url"
	"sync"

	"github.com/spf13/viper"
	"golang.org/x/crypto/ssh"

	"fmt"
)

func init() {
	RegisterFakeSock(func() FakeSock {
		return &DockerSock{Mutex: new(sync.Mutex)}
	})
}

type DockerSock struct {
	*sync.Mutex
	conn   ssh.Conn
	socket string

	ln net.Listener
}

func (gs *DockerSock) IsServing() bool {
	gs.Lock()
	defer gs.Unlock()
	return gs.ln != nil
}

func (gs *DockerSock) SetSocket(conn ssh.Conn, socket string) {
	gs.conn = conn
	gs.socket = socket
}

func (gs DockerSock) GetFileBases() []string {
	return []string{"docker.sock"}
}
func (gs *DockerSock) Close() error {
	if gs.ln == nil {
		return nil
	}
	if err := gs.ln.Close(); err != nil {
		return err
	}
	gs.ln = nil
	return nil
}

func (gs *DockerSock) Serve(ln net.Listener) error {
	gs.ln = ln
	gs.Unlock()
	defer gs.Close()
	for {
		conn, err := ln.Accept()
		if err != nil {
			return err
		}
		if gs.conn != nil {
			gs.forwardConnection(conn)
		} else if viper.GetString("docker.passthrough") == "host" {
			host := viper.GetString("docker.host")
			if host == "" {
				host = "/var/run/docker.sock"
			}
			u, err := url.Parse(host)
			if err != nil {
				return fmt.Errorf("Invalid docker.host: %s", host)
			} else {
				var c net.Conn
				var err error
				if u.Scheme == "" {
					c, err = net.Dial("unix", u.Path)
				} else {
					c, err = net.Dial(u.Scheme, u.Hostname()+":"+u.Port())
				}
				if err != nil {
					return err
				}
				go gs.bridgeConn(c, conn)
				go gs.bridgeConn(conn, c)
			}
		} else if viper.GetString("docker.passthrough") == "none" || viper.GetString("docker.passthrough") == "" {
			conn.Close()
		} else if viper.GetString("docker.passthrough") != "" {
			host := viper.GetString("docker.passthrough")
			u, err := url.Parse(host)
			if err != nil {
				return fmt.Errorf("Invalid docker.host: %s", host)
			} else {
				var c net.Conn
				var err error
				if u.Scheme == "" {
					c, err = net.Dial("unix", u.Path)
				} else {
					c, err = net.Dial(u.Scheme, u.Hostname()+":"+u.Port())
				}
				if err != nil {
					return err
				}
				go gs.bridgeConn(c, conn)
				go gs.bridgeConn(conn, c)
			}
		}
	}
}
func (gs DockerSock) forwardConnection(conn net.Conn) error {
	ch, reqs, err := gs.conn.OpenChannel("forwarded-streamlocal@openssh.com", ssh.Marshal(struct {
		SocketPath string
		Reserved0  string
	}{
		SocketPath: gs.socket,
	}))
	if err != nil {
		return err
	}
	go ssh.DiscardRequests(reqs)
	go gs.bridgeConn(conn, ch)
	go gs.bridgeConn(ch, conn)
	return nil
}
func (gs DockerSock) bridgeConn(c1, c2 io.ReadWriter) {
	defer c1.(io.Closer).Close()
	defer c2.(io.Closer).Close()
	io.Copy(c1, c2)
}
