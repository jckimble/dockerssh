package sock

import (
	"fmt"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
	"io"
	"net"
	"sync"
)

func init() {
	RegisterFakeSock(func() FakeSock {
		return &SshSock{Mutex: new(sync.Mutex)}
	})
}

type SshSock struct {
	*sync.Mutex
	socket string
	conn   ssh.Conn

	ln net.Listener
}

func (gs *SshSock) IsServing() bool {
	gs.Lock()
	defer gs.Unlock()
	return gs.ln != nil
}

func (ss SshSock) SetSocket(conn ssh.Conn, socket string) {
	ss.conn = conn
	ss.socket = socket
}

func (ss SshSock) GetFileBases() []string {
	return []string{"S.gpg-agent.ssh"}
}

func (ss SshSock) bridgeConn(c1, c2 io.ReadWriter) {
	defer c1.(io.Closer).Close()
	defer c2.(io.Closer).Close()
	io.Copy(c1, c2)
}

func (ss *SshSock) Close() error {
	if ss.ln == nil {
		return nil
	}
	if err := ss.ln.Close(); err != nil {
		return err
	}
	ss.ln = nil
	return nil
}

func (ss *SshSock) Serve(ln net.Listener) error {
	ss.ln = ln
	ss.Unlock()
	defer ss.Close()
	for {
		conn, err := ln.Accept()
		if err != nil {
			return err
		}
		if ss.conn == nil {
			if err := agent.ServeAgent(sshAgent{}, conn); err != nil {
				return err
			}
		} else {
			ch, reqs, err := ss.conn.OpenChannel("forwarded-streamlocal@openssh.com", ssh.Marshal(struct {
				SocketPath string
				Reserved0  string
			}{
				SocketPath: ss.socket,
			}))
			if err != nil {
				return err
			}
			go ssh.DiscardRequests(reqs)
			go ss.bridgeConn(conn, ch)
			go ss.bridgeConn(ch, conn)
		}
	}
}

//SSH Agent functions(stub)
type sshAgent struct {
}

func (ss sshAgent) List() ([]*agent.Key, error) {
	return []*agent.Key{}, nil
}

func (ss sshAgent) Signers() ([]ssh.Signer, error) {
	return []ssh.Signer{}, nil
}

func (ss sshAgent) Sign(k ssh.PublicKey, d []byte) (*ssh.Signature, error) {
	return nil, fmt.Errorf("This is a stub")
}

func (ss sshAgent) Add(k agent.AddedKey) error {
	return fmt.Errorf("This is a stub")
}

func (ss sshAgent) Remove(k ssh.PublicKey) error {
	return nil
}

func (ss sshAgent) RemoveAll() error {
	return nil
}

func (ss sshAgent) Lock(p []byte) error {
	return nil
}

func (ss sshAgent) Unlock(p []byte) error {
	return nil
}
