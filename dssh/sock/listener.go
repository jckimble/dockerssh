package sock

import (
	"net"
	"os"
)

type RmListener struct {
	net.Listener
	File string
}

func (rl RmListener) Close() error {
	err := rl.Listener.Close()
	os.Remove(rl.File)
	return err
}
