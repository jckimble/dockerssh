package dssh

import (
	"fmt"
	"io"
	"log"
	"net"
	"path"

	"crypto/rand"
	"crypto/rsa"

	"gitlab.com/jckimble/dockerssh/dssh/key"
	"gitlab.com/jckimble/dockerssh/dssh/sock"
	"gitlab.com/jckimble/dockerssh/locks"
	"gitlab.com/jckimble/dockerssh/models"

	"github.com/docker/docker/client"

	"golang.org/x/crypto/ssh"
)

type Server struct {
	UserDB      models.UserManager
	LockManager locks.LockManager
	Docker      *client.Client

	KeyStorage key.KeyStorage
	Addr       string

	forwards map[sock.Key]sock.FakeSock
	sessions map[ssh.Conn]*Session
	ln       net.Listener
}

func (s *Server) GetSigner() (ssh.Signer, error) {
	if s.KeyStorage != nil {
		b, err := s.KeyStorage.Get()
		if err == nil {
			return ssh.ParsePrivateKey(b)
		}
	}
	key, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, err
	}
	if s.KeyStorage != nil {
		if err := s.KeyStorage.Save(key); err != nil {
			return nil, err
		}
	}
	signer, err := ssh.NewSignerFromKey(key)
	if err != nil {
		return nil, err
	}
	return signer, nil
}

func (dssh Server) PublicKeyAuth(conn ssh.ConnMetadata, key ssh.PublicKey) (*ssh.Permissions, error) {
	keyBytes := ssh.MarshalAuthorizedKey(key)
	user, err := dssh.UserDB.GetByName(conn.User())
	if err != nil {
		log.Printf("Error: %s\n", err)
		return nil, err
	}
	if !user.ValidateSshKey(keyBytes) {
		return nil, fmt.Errorf("Invalid SSH Key")
	}
	if user.IsValid() {
		return nil, nil
	}
	return nil, fmt.Errorf("Invalid SSH Key")
}

func (dssh Server) PasswordAuth(conn ssh.ConnMetadata, password []byte) (*ssh.Permissions, error) {
	user, err := dssh.UserDB.GetByName(conn.User())
	if err != nil {
		log.Printf("Error: %s\n", err)
		return nil, err
	}
	if user.DisablePassword {
		return nil, fmt.Errorf("Invalid Password")
	}
	if dssh.LockManager != nil {
		if dssh.LockManager.IsLocked(user.ID) {
			return nil, fmt.Errorf("Invalid Password")
		}
	}
	if !user.ValidatePassword(string(password)) {
		if dssh.LockManager != nil {
			dssh.LockManager.AddFail(user.ID)
		}
		return nil, fmt.Errorf("Invalid Password")
	}
	if user.IsValid() {
		return nil, nil
	}
	return nil, fmt.Errorf("Invalid Password")
}

func (s *Server) ListenAndServe() error {
	addr := s.Addr
	if addr == "" {
		addr = ":22"
	}
	ln, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}
	return s.Serve(ln)
}
func (s *Server) Serve(l net.Listener) error {
	s.ln = l
	defer l.Close()
	if s.forwards == nil {
		s.forwards = map[sock.Key]sock.FakeSock{}
	}
	if s.sessions == nil {
		s.sessions = map[ssh.Conn]*Session{}
	}
	signer, err := s.GetSigner()
	if err != nil {
		return err
	}
	config := &ssh.ServerConfig{
		PasswordCallback:  s.PasswordAuth,
		PublicKeyCallback: s.PublicKeyAuth,
	}
	config.Config.MACs = []string{"hmac-sha2-256-etm@openssh.com", "hmac-sha2-256", "hmac-sha1"}
	config.AddHostKey(signer)
	for {
		conn, err := l.Accept()
		if err != nil {
			return err
		}
		newConn, chans, reqs, err := ssh.NewServerConn(conn, config)
		if err != nil {
			if err != io.EOF {
				log.Printf("Failed handshake: %s", err)
			}
			continue
		}
		go s.handleRequests(newConn, reqs)
		go s.handleChannels(newConn, chans)
	}
}

type localForwardRequest struct {
	SocketPath string
}
type localForwardedRequest struct {
	SocketPath string
	Reserved0  string
}

func (s *Server) GetForward(key sock.Key) sock.FakeSock {
	if _, ok := s.forwards[key]; !ok {
		s.forwards[key] = sock.GetFakeSock(key.Base)
	}
	return s.forwards[key]
}
func (s *Server) Shutdown() error {
	for k, _ := range s.forwards {
		s.forwards[k].Close()
		delete(s.forwards, k)
	}
	for k, _ := range s.sessions {
		s.sessions[k].Exit(127)
		delete(s.sessions, k)
	}
	return s.ln.Close()
}

func (s *Server) handleRequests(conn ssh.Conn, in <-chan *ssh.Request) {
	for req := range in {
		switch req.Type {
		case "cancel-streamlocal-forward@openssh.com":
			var payload localForwardRequest
			if err := ssh.Unmarshal(req.Payload, &payload); err != nil {
				log.Printf("Payload Error: %s", err)
				if req.WantReply {
					req.Reply(false, nil)
				}
				return
			}
			key := sock.Key{
				User: conn.User(),
				Base: path.Base(payload.SocketPath),
			}
			forward := s.GetForward(key)
			forward.SetSocket(nil, "")
		case "streamlocal-forward@openssh.com":
			var payload localForwardRequest
			if err := ssh.Unmarshal(req.Payload, &payload); err != nil {
				log.Printf("Payload Error: %s", err)
				if req.WantReply {
					req.Reply(false, nil)
				}
				return
			}
			key := sock.Key{
				User: conn.User(),
				Base: path.Base(payload.SocketPath),
			}
			forward := s.GetForward(key)
			nPayload := localForwardedRequest{
				SocketPath: payload.SocketPath,
			}
			if req.WantReply {
				req.Reply(true, ssh.Marshal(nPayload))
			}
			forward.SetSocket(conn, payload.SocketPath)
		default:
			log.Printf("SSH Request: %s", req.Type)
			if req.WantReply {
				req.Reply(false, nil)
			}
		}
	}
}

func (s *Server) handleChannels(conn ssh.Conn, chans <-chan ssh.NewChannel) {
	for newChan := range chans {
		go s.handleChannel(conn, newChan)
	}
}

type localForwardChannelData struct {
	DestAddr string
	DestPort uint32

	OriginAddr string
	OriginPort uint32
}

func (s *Server) handleChannel(conn ssh.Conn, newChan ssh.NewChannel) {
	switch newChan.ChannelType() {
	case "session":
		ch, reqs, err := newChan.Accept()
		if err != nil {
			log.Printf("Handle Channel Error: %s", err)
			return
		}
		sess := &Session{
			UserDB:  s.UserDB,
			Docker:  s.Docker,
			Channel: ch,
			Conn:    conn,
			Server:  s,
		}
		s.sessions[conn] = sess
		sess.handleRequests(reqs)
		delete(s.sessions, conn)
	case "direct-tcpip":
		d := localForwardChannelData{}
		if err := ssh.Unmarshal(newChan.ExtraData(), &d); err != nil {
			newChan.Reject(ssh.ConnectionFailed, "error parsing forward data: "+err.Error())
			return
		}
		if sess, ok := s.sessions[conn]; ok {
			ch, reqs, err := newChan.Accept()
			if err != nil {
				log.Printf("Handle Channel Error: %s", err)
				return
			}
			go ssh.DiscardRequests(reqs)
			sess.forwardPort(d.DestPort, ch)
		} else {
			newChan.Reject(ssh.ConnectionFailed, "Session doesn't exist")
		}
	default:
		newChan.Reject(ssh.UnknownChannelType, fmt.Sprintf("unknown channel type: %s", newChan.ChannelType()))
		log.Printf("unknown channel type: %s", newChan.ChannelType())
	}
}
