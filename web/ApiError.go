package web

import (
	"encoding/json"
	"net/http"
)

type ApiError struct {
	Message string `json:"message"`
	Code    int    `json:"-"`
}

func (a ApiError) Write(w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(a.Code)
	return json.NewEncoder(w).Encode(a)
}

func successMessage(msg string) ApiError {
	return ApiError{
		Code:    200,
		Message: msg,
	}
}

func serverError(msg string) ApiError {
	return ApiError{
		Code:    500,
		Message: msg,
	}
}

func forbidden(msg string) ApiError {
	return ApiError{
		Code:    403,
		Message: msg,
	}
}

func badRequest(msg string) ApiError {
	return ApiError{
		Code:    400,
		Message: msg,
	}
}
func unauthorized(msg string) ApiError {
	return ApiError{
		Code:    400,
		Message: msg,
	}
}

var (
	UnableToGetUser  = serverError("Unable to Get User")
	PermissionDenied = forbidden("You must be Administrator to access this api")
	IdNotInt         = badRequest("id must be an int")
	InvalidLogin     = forbidden("Invalid Email/Password Combination")
	InvalidToken     = unauthorized("Invalid Auth Token")
)
