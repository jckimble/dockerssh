package web

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/jckimble/dockerssh/locks"
	"gitlab.com/jckimble/dockerssh/models"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/spf13/viper"
)

type Auth struct {
	UserDB      models.UserManager
	LockManager locks.LockManager
}

func (wp WebPanel) isAdmin(r *http.Request) (uint, bool) {
	uid := r.Context().Value("id").(uint)
	user, err := wp.UserDB.GetByID(uid)
	if err != nil {
		return 0, false
	}
	if user.Type == "Administrator" {
		return user.ID, true
	}
	return user.ID, false
}

func (a Auth) Register(w http.ResponseWriter, r *http.Request) {
	if !a.UserDB.IsEmpty() && !viper.GetBool("web.registration.enabled") {
		forbidden("Signup is disabled on this instance").Write(w)
		return
	}
	newuser := &models.User{
		Email: r.Form.Get("email"),
		Type:  "Member",
	}
	if a.UserDB.IsEmpty() {
		newuser.Type = "Administrator"
	}
	newuser.SetPassword(r.Form.Get("password"))
	a.UserDB.Save(newuser)
	if err := createJWT(newuser.ID, newuser.Type, true, w); err != nil {
		log.Printf("JWT Error: %s", err)
		serverError("Unable to create login token")
	}
}

func (a Auth) Signin(w http.ResponseWriter, r *http.Request) {
	us, err := a.UserDB.GetByEmail(r.Form.Get("email"))
	if err != nil {
		InvalidLogin.Write(w)
		return
	}
	if a.LockManager != nil {
		if a.LockManager.IsLocked(us.ID) {
			InvalidLogin.Write(w)
			return
		}
	}
	if !us.ValidatePassword(r.Form.Get("password")) {
		if a.LockManager != nil {
			a.LockManager.AddFail(us.ID)
		}
		InvalidLogin.Write(w)
		return
	}
	if err := createJWT(us.ID, us.Type, us.TotpToken == "", w); err != nil {
		log.Printf("JWT Error: %s", err)
		serverError("Unable to create login token")
	}
}

func (a Auth) Authorize(w http.ResponseWriter, r *http.Request) {
	claims, err := parseJWTFromHeader(r)
	if err != nil {
		log.Printf("can't parse jwt")
		InvalidToken.Write(w)
		return
	}
	us, err := a.UserDB.GetByID(uint(claims["id"].(float64)))
	if err != nil {
		log.Printf("invalid user")
		InvalidToken.Write(w)
		return
	}
	if us.ValidateTotp(r.FormValue("code")) {
		if err := createJWT(us.ID, us.Type, true, w); err != nil {
			log.Printf("JWT Error: %s", err)
			serverError("Unable to create login token")
		}
	} else {
		log.Printf("code isn't valid")
		unauthorized("Invalid Totp Code").Write(w)
	}
}

func getJWT(claims jwt.MapClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(viper.GetString("web.jwtkey")))

}

func createJWT(uid uint, utype string, authorized bool, w http.ResponseWriter) error {
	claims := jwt.MapClaims{
		"id":         uid,
		"type":       utype,
		"authorized": authorized,
		"exp":        time.Now().Add(2 * time.Hour).Unix(),
		"iat":        time.Now().Unix(),
	}
	tokenString, err := getJWT(claims)
	if err != nil {
		return err
	}
	data := struct {
		Token string `json:"token"`
	}{
		Token: tokenString,
	}
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(data); err != nil {
		return err
	}
	return nil
}

func parseJWTFromHeader(r *http.Request) (jwt.MapClaims, error) {
	spt := strings.Split(r.Header.Get("Authorization"), "Bearer ")
	if len(spt) != 2 {
		return nil, fmt.Errorf("Invalid Header")
	}
	return parseJWT(spt[1])
}

func parseJWT(tok string) (jwt.MapClaims, error) {
	token, err := jwt.Parse(tok, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(viper.GetString("web.jwtkey")), nil
	})
	if err != nil {
		return nil, err
	}
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, nil
	} else {
		return nil, fmt.Errorf("Invalid Token")
	}
}

type AuthHandler struct {
	Handler http.Handler
}

func (a AuthHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	claims, err := parseJWTFromHeader(r)
	if err != nil {
		InvalidToken.Write(w)
		return
	}
	if authorized, ok := claims["authorized"].(bool); ok && authorized {
		c := context.WithValue(r.Context(), "id", uint(claims["id"].(float64)))
		a.Handler.ServeHTTP(w, r.WithContext(c))
	} else {
		unauthorized("Not Authorized").Write(w)
	}
}
