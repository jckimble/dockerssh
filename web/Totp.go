package web

import (
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"github.com/pquerna/otp/totp"
	"net/http"
	"strconv"
	"time"

	"log"
)

func (wp WebPanel) GenerateTotp(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		IdNotInt.Write(w)
		return
	}
	if i, ok := wp.isAdmin(r); !ok && i != uint(id) {
		PermissionDenied.Write(w)
		return
	}
	us, err := wp.UserDB.GetByID(uint(id))
	if err != nil {
		UnableToGetUser.Write(w)
		return
	}
	if us.TotpToken != "" {
		badRequest("Totp Token Already Exists").Write(w)
		return
	}
	k, err := totp.Generate(totp.GenerateOpts{
		Issuer:      "dockerssh",
		AccountName: "dockerssh",
	})
	if err != nil {
		serverError("Unable To Create Secret").Write(w)
		return
	}
	claims := jwt.MapClaims{
		"id":        us.ID,
		"secret":    k.Secret(),
		"period":    30,
		"algorithm": "SHA1",
		"digits":    6,
		"exp":       time.Now().Add(5 * time.Minute).Unix(),
		"iat":       time.Now().Unix(),
	}
	token, err := getJWT(claims)
	if err != nil {
		serverError("Unable To Create Secret").Write(w)
		return
	}
	data := struct {
		SignedSecret string `json:"signedSecret"`
	}{
		SignedSecret: token,
	}
	json.NewEncoder(w).Encode(data)
}

func (wp WebPanel) ActivateTotp(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		IdNotInt.Write(w)
		return
	}
	if i, ok := wp.isAdmin(r); !ok && i != uint(id) {
		PermissionDenied.Write(w)
		return
	}
	data, err := wp.UserDB.GetByID(uint(id))
	if err != nil {
		UnableToGetUser.Write(w)
		return
	}
	if data.TotpToken != "" {
		badRequest("Totp Token Already Exists").Write(w)
		return
	}
	if r.FormValue("code") == "" || r.FormValue("signedSecret") == "" {
		badRequest("code and signedSecret are required").Write(w)
		return
	}
	claims, err := parseJWT(r.FormValue("signedSecret"))
	if err != nil {
		badRequest("Invalid signedSecret").Write(w)
		return
	}
	if secret, ok := claims["secret"].(string); ok {
		if !totp.Validate(r.FormValue("code"), secret) {
			log.Printf("%s %s", r.FormValue("code"), secret)
			badRequest("Invalid Totp Code").Write(w)
			return
		}
		data.TotpToken = secret
	} else {
		badRequest("Invalid signedSecret").Write(w)
		return
	}
	if err := wp.UserDB.Save(data); err != nil {
		serverError("Unable to Activate Totp Token").Write(w)
		return
	}
	successMessage("Totp Token Activated").Write(w)
}

func (wp WebPanel) RemoveTotp(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		IdNotInt.Write(w)
		return
	}
	if i, ok := wp.isAdmin(r); !ok && i != uint(id) {
		PermissionDenied.Write(w)
		return
	}
	data, err := wp.UserDB.GetByID(uint(id))
	if err != nil {
		UnableToGetUser.Write(w)
		return
	}
	data.TotpToken = ""
	if err := wp.UserDB.Save(data); err != nil {
		serverError("Unable to Remove Totp Token").Write(w)
		return
	}
	successMessage("Totp Token Removed").Write(w)
}
