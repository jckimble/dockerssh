package web

import (
	"encoding/json"
	"gitlab.com/jckimble/dockerssh/models"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"golang.org/x/crypto/ssh"
	"strconv"
)

type WebPanel struct {
	UserDB models.UserManager
}

func (wp WebPanel) AddUser(w http.ResponseWriter, r *http.Request) {
	if _, ok := wp.isAdmin(r); !ok {
		PermissionDenied.Write(w)
		return
	}
	if r.Form.Get("email") == "" || r.Form.Get("newpass") == "" {
		badRequest("Email and Password are required").Write(w)
		return
	}
	u := &models.User{
		Email:           r.Form.Get("email"),
		Image:           r.Form.Get("image"),
		FirstName:       r.Form.Get("firstname"),
		LastName:        r.Form.Get("lastname"),
		UserName:        r.Form.Get("username"),
		DisablePassword: r.Form.Get("disablePassword") == "yes",
		Type:            r.Form.Get("type"),
	}
	u.SetPassword(r.Form.Get("newpass"))
	if err := wp.UserDB.Save(u); err != nil {
		serverError("Unable to add user").Write(w)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(u); err != nil {
		log.Printf("JSONEncode Error: %s", err)
	}
}

func (wp WebPanel) GetUsers(w http.ResponseWriter, r *http.Request) {
	if _, ok := wp.isAdmin(r); !ok {
		PermissionDenied.Write(w)
		return
	}
	data, err := wp.UserDB.GetAll()
	if err != nil {
		UnableToGetUser.Write(w)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(data); err != nil {
		log.Printf("JSONEncode Error: %s", err)
	}
}

func (wp WebPanel) DeleteUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		IdNotInt.Write(w)
		return
	}
	i, ok := wp.isAdmin(r)
	if !ok && i != uint(id) {
		PermissionDenied.Write(w)
		return
	}
	if i == uint(id) {
		forbidden("You cannot delete yourself").Write(w)
		return
	}
	if err := wp.UserDB.Delete(uint(id)); err != nil {
		UnableToGetUser.Write(w)
		return
	}
	successMessage("Deleted User").Write(w)
}
func (wp WebPanel) GetUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		IdNotInt.Write(w)
		return
	}
	if i, ok := wp.isAdmin(r); !ok && i != uint(id) {
		PermissionDenied.Write(w)
		return
	}
	data, err := wp.UserDB.GetByID(uint(id))
	if err != nil {
		UnableToGetUser.Write(w)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(data); err != nil {
		log.Printf("JSONEncode Error: %s", err)
	}
}
func (wp WebPanel) EditUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		IdNotInt.Write(w)
		return
	}
	if i, ok := wp.isAdmin(r); !ok && i != uint(id) {
		PermissionDenied.Write(w)
		return
	}
	data, err := wp.UserDB.GetByID(uint(id))
	if err != nil {
		UnableToGetUser.Write(w)
		return
	}
	if aid, ok := wp.isAdmin(r); ok && aid != uint(id) && r.Form.Get("type") != "" {
		data.Type = r.Form.Get("type")
	}
	if r.Form.Get("lastname") != "" {
		data.LastName = r.Form.Get("lastname")
	}
	if r.Form.Get("username") != "" {
		data.UserName = r.Form.Get("username")
	}
	if r.Form.Get("firstname") != "" {
		data.FirstName = r.Form.Get("firstname")
	}
	if r.Form.Get("image") != "" {
		data.Image = r.Form.Get("image")
	}
	if r.Form.Get("email") != "" {
		data.Email = r.Form.Get("email")
	}
	if r.Form.Get("newpass") != "" {
		data.SetPassword(r.Form.Get("newpass"))
	}
	if r.Form.Get("disablePassword") != "" {
		data.DisablePassword = r.Form.Get("disablePassword") == "yes"
	}
	if err := wp.UserDB.Save(data); err != nil {
		serverError("Unable to update user").Write(w)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(data); err != nil {
		log.Printf("JSONEncode Error: %s", err)
	}
}

func (wp WebPanel) NotFound(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s %+v", r.Method, r.URL)
	log.Printf("%+v", r.Form)
	ApiError{
		Code:    404,
		Message: "Endpoint Not Found! Make Sure Server and WebPages Match!",
	}.Write(w)
}

func (wp WebPanel) AddUserKey(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		IdNotInt.Write(w)
		return
	}
	if i, ok := wp.isAdmin(r); !ok && i != uint(id) {
		PermissionDenied.Write(w)
		return
	}
	data, err := wp.UserDB.GetByID(uint(id))
	if err != nil {
		UnableToGetUser.Write(w)
		return
	}
	if r.Form.Get("name") == "" || r.Form.Get("key") == "" {
		badRequest("Name and Key are required").Write(w)
		return
	}
	publicKey, _, _, _, err := ssh.ParseAuthorizedKey([]byte(r.Form.Get("key")))
	if err != nil {
		badRequest("Key must be a SSH Public Key").Write(w)
		return
	}
	key := &models.Key{
		UserID: data.ID,
		Name:   r.Form.Get("name"),
		Data:   publicKey.Marshal(),
		Date:   time.Now(),
	}
	if err := key.GenerateFingerprint(); err != nil {
		serverError("Unable to get fingerprint of key").Write(w)
		return
	}
	data.AddKey(key)
	if err := wp.UserDB.Save(data); err != nil {
		serverError("Unable to save key").Write(w)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(key); err != nil {
		log.Printf("JSONEncode Error: %s", err)
	}
}
func (wp WebPanel) GetUserKeys(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		IdNotInt.Write(w)
		return
	}
	if i, ok := wp.isAdmin(r); !ok && i != uint(id) {
		PermissionDenied.Write(w)
		return
	}
	data, err := wp.UserDB.GetByID(uint(id))
	if err != nil {
		UnableToGetUser.Write(w)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(data.GetKeys()); err != nil {
		log.Printf("JSONEncode Error: %s", err)
	}
}

func (wp WebPanel) DeleteKey(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		IdNotInt.Write(w)
		return
	}
	if i, ok := wp.isAdmin(r); !ok && i != uint(id) {
		PermissionDenied.Write(w)
		return
	}
	data, err := wp.UserDB.GetByID(uint(id))
	if err != nil {
		UnableToGetUser.Write(w)
		return
	}
	data.RemoveKey(params["fingerprint"])
	if err := wp.UserDB.Save(data); err != nil {
		log.Printf("Delete Error: %s", err)
		serverError("Unable to delete key").Write(w)
		return
	}
	successMessage("Key Removed").Write(w)
}
