package web

import (
	"encoding/json"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

type JSONDataHandler struct {
	Handler http.Handler
}

func (j JSONDataHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if strings.Contains(r.Header.Get("Content-Type"), "application/json") {
		defer r.Body.Close()
		r.ParseForm()
		data := map[string]interface{}{}
		if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
			log.Printf("JSONDecode Error: %+v", err.Error())
			badRequest("Json Data Expected").Write(w)
			return
		}
		if r.Form == nil {
			r.Form = url.Values{}
		}
		for n, v := range data {
			if val, ok := v.(string); ok {
				r.Form.Add(n, val)
			} else if val, ok := v.(bool); ok {
				if val {
					r.Form.Add(n, "yes")
				} else {
					r.Form.Add(n, "no")
				}
			} else if val, ok := v.(float64); ok {
				r.Form.Add(n, strconv.Itoa(int(val)))
			} else {
				log.Printf("Unsupported Type: %+v", v)
			}
		}
	}
	j.Handler.ServeHTTP(w, r)
}
